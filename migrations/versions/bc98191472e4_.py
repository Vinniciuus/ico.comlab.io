"""empty message

Revision ID: bc98191472e4
Revises: e69bb9672ebc
Create Date: 2023-01-18 01:07:31.825295

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'bc98191472e4'
down_revision = 'e69bb9672ebc'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('produto', sa.Column('img_filename', sa.String(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('produto', 'img_filename')
    # ### end Alembic commands ###
