from flask import Blueprint
from flask import render_template
from models import Post
from flask_security import login_required
from flask import request
from config import Config
from flask import redirect
from flask import url_for
from models import Post, Product
from app import app, db
app.app_context().push()
from flask import render_template, flash
from models import User
from home.forms import LoginForm, LoginFormAdd
from flask_login import login_user
from models import Product


blueprint_instance = Blueprint('home', __name__, template_folder='templates')



@blueprint_instance.route('/')
def home():
    

    fp = request.args.get('fp')
    print(fp)
    if fp:
        
        data = Product.query.filter(Product.cnpj.contains(fp) | 
        Product.cnpj.contains(fp)).all()
    else:
        data = Product.query.order_by(Product.created.desc())
        data = Product.query.all()
    return render_template('home.html', data=data)
    
    
@blueprint_instance.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    user = User.query.filter_by(email=form.username.data).first()
    print(user)
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.username.data).first()
        print(user)
        
        if not user or  not user.verify_password(form.password.data):
            flash("Login invalido")
        else:
            login_user(user)
            flash("Logado")
            print("Logou")
            return  render_template('home.html')
    print("Nao Logou")
    return render_template('login.html', form=form)


