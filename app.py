from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate  import Migrate, MigrateCommand
from flask_script import Manager
from config import Config
from flask_admin import Admin
from flask_admin import AdminIndexView
from flask_admin.contrib.sqla import ModelView
from flask_admin.contrib.fileadmin import FileAdmin
from flask_security import SQLAlchemyUserDatastore
from flask_security import Security
from flask_security import current_user
from flask import redirect, url_for, request
from flask_login import LoginManager



app = Flask(__name__)
app.config.from_object(Config)


db = SQLAlchemy(app)


lm = LoginManager()
lm.init_app(app)
from models import *

migrate = Migrate(app, db)
manager = Manager(app)

manager.add_command('db', MigrateCommand)




class AdminMixin:
    def is_accessible(self):
        return current_user.has_role('admin')
        
    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for('security.login', next=request.url))


class AdminView(ModelView):
    pass


class HomeAdminView(AdminIndexView):
    pass
    
    
class BaseModelView(ModelView):
    def on_model_change(self, form, model, is_created):
        if is_created:
            model.generate_slug()
            
        return super().on_model_change(form, model, is_created)


class PostAdminView(AdminMixin, BaseModelView):
    form_columns = ['title', 'body', 'tags']
    

class TagAdminView(AdminMixin, BaseModelView):
    pass
    
class RoleAdminView(AdminMixin, BaseModelView):
    pass
    
class UserAdminView(AdminMixin, BaseModelView):
    pass
    
class ProdutoAdminView(AdminMixin, ModelView):
    pass
class FornecedorAdminView(AdminMixin, ModelView):
    pass
    
class FornecedorAdminView(AdminMixin, ModelView):
    pass
    
class ProductAdminView(AdminMixin, ModelView):
    pass
    
    
    


admin = Admin(app, 'Aplicação ICO', url='/', index_view=HomeAdminView(name=''))

path = op.join(op.dirname(__file__), 'static')
admin.add_view(FileAdmin(path, '/static/', name='Static Files'))


admin.add_view(PostAdminView(Post, db.session))
admin.add_view(ProductAdminView(Product, db.session))
admin.add_view(TagAdminView(Tag, db.session))
admin.add_view(RoleAdminView(Role, db.session))
admin.add_view(UserAdminView(User, db.session))
admin.add_view(ProdutoAdminView(Produto, db.session))
admin.add_view(FornecedorAdminView(Fornecedor, db.session))

# Flask-Security

user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security = Security(app, user_datastore)
