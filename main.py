from app import app,db
import views
from home.home import blueprint_instance as bl_print
from flask import Blueprint, render_template, request, url_for,redirect
from config import Config
import os
from models import Product


url = Config()
app.register_blueprint(bl_print)


@app.route('/upload_img', methods=['GET', 'POST'])
def upload_img():
    data = Product.query.all()
  
    if request.method=="POST":
        name = request.form['name']
        cnpj = request.form['cnpj']
        endereco = request.form['endereco']
        upload_image = request.files['upload_image']
        if upload_image.filename != "":
            filepath=os.path.join(url.UPLOAD_FOLDER, upload_image.filename)
            upload_image.save(filepath)
            product = Product(name, cnpj, endereco, upload_image.filename)
            db.session.add(product)
            db.session.commit()
            
            data = Product.query.all()
            return render_template("upload.html",data=data)
      
       
    return render_template("upload.html",data=data)

@app.route('/delete_product/<int:id>')
def delete_product(id):
    try:
        product = Product.query.filter(Product.id==id).first()
        db.session.delete(product)
        db.session.commit()
        
    except:
       
        flash("Record Deleted Failed", "danger")
   
    finally:
        return redirect(url_for("upload_img"))
       



@app.route('/home_pesquisa')
def home():

    fp = request.args.get('fp')
    print(fp)
    if fp:
        
        data = Product.query.filter(Product.cnpj.contains(fp) | 
        Product.cnpj.contains(fp)).all()
    else:
        data = Product.query.order_by(Product.created.desc())
        data = Product.query.all()
    return render_template('home.html', data=data)
    


if __name__=='__main__':
    app.run()