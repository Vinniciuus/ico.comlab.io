from app import db, lm
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import  UserMixin
import os.path as op
from datetime import date
import re
from datetime import datetime


from flask_security import UserMixin, RoleMixin

@lm.user_loader
def get_user(user_id):
    return User.query.filter_by(id=user_id).first()


def slugify(s):
    pattern = r'[^\w+]'
    return re.sub(pattern, '-', s)
    
posts_tags = db.Table('posts_tags',
                      db.Column('post_id', db.Integer,
                      db.ForeignKey('post.id')),
                      db.Column('tag_id', db.Integer,
                      db.ForeignKey('tag.id'))              
)


roles_users = db.Table('roles_users',
                      db.Column('user_id', db.Integer,
                      db.ForeignKey('user.id')),
                      db.Column('role_id', db.Integer,
                      db.ForeignKey('role.id'))              
)


class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(140))
    slug = db.Column(db.String(140), unique=True)
    body = db.Column(db.Text)
    created = db.Column(db.DateTime, default=datetime.now())
    tags =  db.relationship('Tag', secondary=posts_tags, backref=db.backref('posts'), lazy='dynamic')
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.generate_slug()
        
    def generate_slug(self):
        if self.title:
            self.slug = slugify(self.title)
            
        else:
            self.slug = str(int(time()))
            
    def __repr__(self):
        return f'<Post id: {self. id}, title: {self.title}'
    
    
class Tag(db.Model):
    id  = db.Column(db.Integer, primary_key=True)
    title  = db.Column(db.String(140))
    slug  = db.Column(db.String(140), unique=True)
    
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.slug = self.generate_slug()
    
    
    def generate_slug(self):
        if self.title:
            self.slug = slugify(self.title)
            
        else:
            self.slug = str(int(time()))
 
    
    def __repr__(self):
        return f'<Tag id: {self.id}, title: {self.title}>'
        
 
 
class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(255))
    active = db.Column(db.Boolean)
    roles = db.relationship('Role', secondary=roles_users, 
    backref=db.backref('users'), lazy='dynamic')
    
    
    def __init__(self, email, password, active):
        self.email = email
        self.password = generate_password_hash(password)
        self.active = True
      
        
    def verify_password(self, pwd):
        return check_password_hash(self.password, pwd)
        
    def __repr__(self):
        return "<User %r>" % self.email
    
    
    
    
class Role(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), unique=True)
    
    def __repr__(self):
        return f'<Role id: {self.id}, Name: {self.name}>'
    
    
class Produto(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    Nome = db.Column(db.String(150))
    img_filename = db.Column(db.String())
    fornecedor = db.Column(db.String(150))
    preco = db.Column(db.Float)
    descricao = db.Column(db.String(400))
    picture = db.Column(db.LargeBinary)
    created = db.Column(db.DateTime, default=datetime.now())
    
    
    def __init__(self, Nome, fornecedor, preco, descricao, picture):
        self.id = id
        self.Nome = Nome
        self.fornecedor = fornecedor
        self.preco = preco
        self.descricao = descricao
        self.picture = picture
        
    
    def __repr__(self):
        return '<image id={},name={}>'.format(self.id, self.Nome)
        
        
       


    
class Fornecedor(db.Model, RoleMixin):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    cnpj = db.Column(db.String(50), unique=True)
    endereco = db.Column(db.String(100))
    created = db.Column(db.DateTime, default=datetime.now())
    
    
    def __repr__(self):
        return f'<Fornecedor id: {self.id}, name: {self.name}>'
        
        
        


class Product(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    cnpj = db.Column(db.String(50), unique=True)
    endereco = db.Column(db.String(100))
    img = db.Column(db.TEXT())
    created = db.Column(db.DateTime, default=datetime.now())
    
    def __init__(self, name, cnpj, endereco, img):
        
        
        self.name = name
        self.cnpj = cnpj
        self.endereco = endereco
        self.img = img
        
        
    def __repr__(self):
        return f'<Product id: {self.id}, name: {self.name}>' 